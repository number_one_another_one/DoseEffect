#include <StdAfx.h>
#include "Utils.h"

std::string Utils::LoadTextFromFile(const std::string& filename)
{
	FILE* file = fopen(filename.c_str(), "rb");
	if (file == nullptr)
	{
		throw std::runtime_error("File " + filename + " not found");
	}

	fseek(file, 0, SEEK_END);
	size_t fileSize = ftell(file);
	fseek(file, 0, SEEK_SET);

	std::string result;
	result.resize(fileSize);
	fread((void*)result.c_str(), 1, fileSize, file);

	fclose(file);

	return result;
}

glm::vec4 Utils::RandomColor()
{	
	return glm::vec4(rand() / (float)RAND_MAX, 
		rand() / (float)RAND_MAX, 
		rand() / (float)RAND_MAX, 
		rand() / (float)RAND_MAX);
}

uint32_t Utils::ColorFromHexString(const std::string& hexString)
{
	std::string strippedString(hexString.begin() + 1, hexString.end());
	uint32_t result = (uint32_t)std::stoi(strippedString, 0, 16);
	return result;
}

glm::vec4 Utils::UnormToFloatColor(uint32_t color)
{	
	return glm::vec4((color >> 16) & 0xFF,
		(color >> 8) & 0xFF,
		color & 0xFF,
		(color >> 24) & 0xFF) / glm::vec4(255.0f);
}

GLuint Utils::CompileProgramFromFiles(const std::string& vertexShaderFilename, const std::string& pixelShaderFilename)
{
	const std::string vertexShaderSourceText = Utils::LoadTextFromFile(vertexShaderFilename);
	const char* vertexShaderSource = vertexShaderSourceText.c_str();

	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &vertexShaderSource, nullptr);
	glCompileShader(vertexShader);

	const std::string pixelShaderSourceText = Utils::LoadTextFromFile(pixelShaderFilename);
	const char* pixelShaderSource = pixelShaderSourceText.c_str();
	GLuint pixelShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(pixelShader, 1, &pixelShaderSource, nullptr);
	glCompileShader(pixelShader);

	GLuint program = glCreateProgram();

	glAttachShader(program, vertexShader);
	glAttachShader(program, pixelShader);
	glLinkProgram(program);

	GLint linkStatus = GL_FALSE;
	glGetProgramiv(program, GL_LINK_STATUS, &linkStatus);
	if (!linkStatus)
	{
		char log[1024];
		glGetProgramInfoLog(program, 1024, nullptr, log);
		std::cout << std::string(log) << std::endl;
	}
	check(linkStatus == GL_TRUE);

	return program;
}