#include <StdAfx.h>
#include "Kernels.h"

static std::uniform_real_distribution<double> distribution;
static std::default_random_engine generator;

double step(double edge, double x)
{
	return x >= edge ? 1.0 : 0.0;
}

double EpanechkinKernel(double x)
{
	double I = fabs(x) <= 1.0 ? 1.0 : 0.0;
	return 3.0 / 4.0 * (1 - x * x) * I;
}

double QuarticKernel(double x)
{
	double I = fabs(x) <= 1.0 ? 1.0 : 0.0;
	return 15.0 / 16.0 * (1.0 - x * x) * (1.0 - x * x) * I;
}

double UniformKernel(double x)
{
	return 0.5 * step(fabs(x), 1.0);
}

double TriangleKernel(double x)
{
	return (1.0 - fabs(x)) * step(fabs(x), 1.0);
}

double CosineKernel(double x)
{
	return (M_PI / 4.0) * cos(M_PI * x / 2.0) * step(fabs(x), 1.0);
}

double LaplaceKernel(double x)
{
	return 0.5 * exp(-fabs(x)) * step(fabs(x), 1.0);
}

double GaussKernel(double x)
{
	return (1.0 / sqrt(2.0 * M_PI)) * exp(-(x * 2) * 0.5) * step(fabs(x), 1.0);
}

Kernel GetKernelByIndex(size_t kernelIndex)
{
	switch (kernelIndex)
	{
		case 0: return EpanechkinKernel;
		case 1: return QuarticKernel;
		case 2: return UniformKernel;
		case 3: return TriangleKernel;
		case 4: return CosineKernel;
		case 5: return LaplaceKernel;
		case 6: return GaussKernel;
	}
	return Kernel();
}
