#include <StdAfx.h>

#include <Application.h>

bool Application::OnInit()
{
	if (!wxApp::OnInit())
	{
		return false;
	}

	mMainWindow = new MainWindow();
	mMainWindow->Show();
	return true;
}

wxIMPLEMENT_APP(Application);