#include "DataModel.h"

void DataModel::GenerateRandomData(double minX, double maxX, double minU, double maxU, size_t dataSize)
{
	mData.clear();

	std::uniform_real_distribution<> UDistribution(minU, maxU);
	std::uniform_real_distribution<> XDistribution(minX, maxX);
	std::uniform_real_distribution<> EpsDistribution(15.0, 15.0);
	std::default_random_engine randomEngine(std::chrono::system_clock::now().time_since_epoch().count());

	for (size_t i = 0; i < dataSize; ++i)
	{
		Data data{};

		data.U = UDistribution(randomEngine);
		data.X = XDistribution(randomEngine);
		data.Eps = EpsDistribution(randomEngine);
		data.W = data.U >= data.X ? 1.0 : 0.0;

		mData.push_back(data);
	}

	std::sort(mData.begin(), mData.end(), [](const Data& x, const Data& y) -> bool
	{
		return x.U < y.U;
	});
}

void DataModel::LoadFromFile(const std::string& filename)
{
	std::ifstream infile(filename);
	std::string line;

	while (std::getline(infile, line))
	{
		std::istringstream iss(line);
		Data data{};
		iss >> data.U >> data.X;
		data.W = data.U >= data.X ? 1.0 : 0.0;
	}
}

void DataModel::SaveToFile(const std::string& filename)
{
	std::ofstream outfile(filename);

}
